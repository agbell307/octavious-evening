#!/bin/bash

##################
#
# Create a feed of Octavius Winston's Daily Morning Readings
#
##################

##################
#
# Update Podcast
#
##################

M_DAY=$(date +"%B%d")
M_DATE=$(date +"%Y%m%d")
CONTENT=$(cat /var/www/html/bach/octavious/evening-readings/$M_DAY)

cat > /tmp/ODR-evening.txt <<- _EOF_
        <item>
                <title>Evening Thoughts by Octavious Winslow</title>
                <link>http://bach.bellclan.xyz/octavious/evening-readings/$M_DAY </link>
                <guid>ODRE_$M_DATE</guid>
                <description><![CDATA[
			 $CONTENT 
			]]>
		</description>
        </item>
_EOF_

#sed -i '44,+6d' /home/aaron/AIO/KVIP/FeedKVIP.xml
#sed -i '8r /tmp/ODR-evening.txt' FeedODRE.xml
sed -i '8r /tmp/ODR-evening.txt' /home/aaron/html/bach/FeedODRM.xml

