#!/bin/bash

MONTH=December
FILE_N=DECEMBER
FILE_E=ev12

csplit --elide-empty-files --prefix=$MONTH source/$FILE_E.htm  '/'$FILE_N'/' {*}
for file in $(find . -name "$MONTH*"); do cat -e $file | sed 's/M-^W/--/g' | sed 's/M-^S/\&quot;/g' | sed 's/M-^T/\&quot;/g' | sed 's/M-^R/\&rsquo;/g' > readings/$file; done
