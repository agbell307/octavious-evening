    APRIL 17. <br>
    <br>
    God, that comforts those that are cast down. 2 Cor. 7:6<br>
    <br>
    IF there is much to cast down the child of God, there is more to lift him 
    up. If in his path to glory there are many causes of soul-despondency, of 
    heart-sorrow, and mental disquietude, yet in that single truth--God comforts 
    the disconsolate--he has an infinite counterbalance of consolation, joy, and 
    hope. That &quot;God comforts those that are cast down,&quot; His own truth declares. 
    It is in His heart to comfort them, and it is in His power to comfort them. 
    He blends the desire, deep and yearning, with the ability, infinite and 
    boundless. Not so with the fondest, tenderest creature. The sorrow is often 
    too deep and too sacred for human sympathy to reach. But what is fathomless 
    to man is a shallow to God. <br>
    <br>
    I have said, that it is in the heart of God to comfort His people. 
    Everything that He has done to promote their comfort proves it. He has 
    commanded His ministers to &quot;speak comfortably&quot; to them. He has sent forth 
    His word to comfort them. He has laid up all comfort and consolation for 
    them, in the Son of His love. And in addition to all this, He has given them 
    His own Spirit, to lead them to the Divine sources of &quot;all consolation&quot; 
    which He has provided. Who could comfort the disconsolate but God? Who could 
    effectually undertake their case but Himself? He only knows their sorrow, 
    and He only could meet it. There is not a moment in which God is not bent 
    upon the comfort of &quot;those that are cast clown.&quot; All His dealings with them 
    tend to this--even those that appear adverse and contrary. Does He wound?--it 
    is to heal. Does He cause deep sorrow?--it is to turn that sorrow into a 
    deeper joy. Does He empty?--it is to fill. Does He cast down?--it is to lift 
    up again. Such is the love that moves Him, such is the wisdom that guides 
    Him, and such too is the end that is secured in the Lord's disciplinary 
    conduct with His people. Dear reader, it is in God's loving heart to speak 
    comfortably to your sorrowful heart. Let but the Holy Spirit enable you to 
    receive this truth in simple faith, and your grief, be its cause and its 
    degree what they may, is more than half assuaged. Not a word may yet be 
    spoken by the &quot;God of all comfort,&quot; not a cloud may be dispersed, nor a 
    difficulty be removed; yet to be assured by the Divine Comforter that the 
    heart of God yearns over you, and that consolation is sparkling up from its 
    infinite depths, waiting only the command to pour its tide of joyousness 
    into your sorrow-stricken bosom, and it is enough. Yes, I repeat it--for 
    every reiteration of so precious a truth must still be but a faint 
    expression of its magnitude--it is in the loving heart of God to lift up your 
    disconsolate soul from the dust. Listen to His words--there is melody in them 
    such as David's harp spoke not when its soft and mellow strains soothed the 
    perturbed spirit of Saul--&quot;I, even I, am He that comforts you.&quot; Mark with 
    what earnestness He makes this declaration. How solicitous does he appear to 
    impress this truth upon the heart--that to comfort His own tried saints is 
    His sole prerogative, and His infinite delight. &quot;I, even I, am He that 
    comforts you.&quot; <br>
    <br>
