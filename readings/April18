    APRIL 18.<br>
    <br>
    For our gospel came not unto you in word only, but also in power, and in the 
    Holy Spirit, and in much assurance; . . . And you became followers of us, 
    and of the Lord, having received the word in much affliction, with joy of 
    the Holy Spirit. 1 Thes. 1:5, 6<br>
    <br>
    THUS does the Spirit of God empty the soul, preparing it for the reception 
    of the grace of Christ. He 'sweeps and garnishes' the house. He dislodges 
    the unlawful inhabitant, dethrones the rival sovereign, and thus secures 
    room for the Savior. He disarms the will of its rebellion against God, the 
    mind of its ignorance, and the heart of its hatred. He prostrates the 
    barrier, removes the veil, and unlocks the door, at which the Redeemer 
    triumphantly enters. In effecting this mighty work, He acts as the Divine 
    Forerunner of Christ. What the Baptist was to our Lord, &quot;crying in the 
    wilderness, Prepare you the way of the Lord,&quot; the Holy Spirit is, in 
    heralding the entrance of Jesus to the soul. He goes before, and prepares 
    His way. The Divinity of the Spirit furnishes Him with all the requisites 
    for the work. He meets with difficulty, and He removes it--with obstruction, 
    and He overcomes it--with opposition, and He vanquishes it. His power is 
    omnipotent, His influence is irresistible, His grace is efficacious. There 
    is no soul, however filled with darkness, and enmity, and rebellion, which 
    He cannot prepare for Christ. There is no heart of stone which He cannot 
    break, no brazen wall which He cannot prostrate, no mountain which He cannot 
    level. Oh, for more faith in the power of the Holy Spirit in the soul of 
    man! How much do we limit, and in limiting how do we dishonor, Him in His 
    work of converting grace! <br>
    <br>
    The providential dealings of God are frequently instrumental in the hand of 
    the Holy Spirit of accomplishing this emptying process, thus preparing the 
    soul for the reception of Christ. The prophet thus strikingly alludes to it: 
    &quot;Moab has been at ease from his youth, and He has settled on his lees, and 
    has not been emptied from vessel to vessel.&quot; It was in this way God dealt 
    with Naomi. Listen to her touching words: &quot;I went out full, and the Lord has 
    brought me home again empty.&quot; Thus it is that the bed of sickness, or the 
    chamber of death, the loss of creature good, perhaps the loveliest and the 
    dearest, has prepared the heart for Christ. The time of bereavement and of 
    solitude, of suffering and of loss, has been the Lord's time of love. 
    Providence is the hand-maid of grace--and God's providential dealings with 
    man are frequently the harbingers of the kingdom of grace in the soul. Ah! 
    how many whose glance falls upon this page may testify &quot;Even thus has the 
    Lord dealt with me. I was rich, and He has impoverished me. I was exalted, 
    and He has laid me low. Not one cup only did He drain, not one vessel only 
    did He dash to the earth, but many. He has emptied me 'from vessel to 
    vessel.' &quot; Happy shall you be if the result of all this emptying and 
    humbling shall be the filling and enriching of your soul with larger 
    communications of grace and truth from Jesus. A cloud of witnesses around 
    you testify to this invariable principle of the Lord's procedure with His 
    people--that He enriches by impoverishing them; strengthens by weakening 
    them; replenishes by emptying; and exalts by laying them low.<br>
    <br>
    <br>
