    APRIL 20.<br>
    <br>
    When the even was come, they brought unto him many that were possessed with 
    devils: and he cast out the spirits with his word, and healed all that were 
    sick: that it might be fulfilled which was spoken by Elijah the prophet, 
    saying, Himself took our infirmities, and bare our sicknesses. Matthew 8:16, 
    17<br>
    <br>
    IN one respect only may it be said, that our Divine and adorable Lord would 
    seem to have been exempted from the physical infirmities peculiar to the 
    nature which He so voluntarily and entirely assumed--it does not appear that 
    He was ever, in His own person, the subject of sickness or disease. It is 
    indeed declared by His inspired biographer, thus confirming at the same time 
    a prediction of one of the prophets, &quot;Himself took our infirmities, and bare 
    our sicknesses;&quot; but this He did in the same manner in which He bore our 
    moral sicknesses, without any personal participation. He bore our sins, but 
    He was Himself sinless. He carried our sicknesses, but He Himself was a 
    stranger to disease. And His exemption from the one will explain His 
    exemption from the other. His humanity knew no sin; it was that &quot;holy thing&quot; 
    begotten by the Holy Spirit, and as stainless as God Himself. As sin 
    introduced into our nature every kind of physical evil, and disease among 
    the rest, our Lord's freedom from the cause necessarily left Him free from 
    the effect. He was never sick, because He never sinned. No, He had never 
    died, had He not consented to die. With a nature prepared and conceived 
    totally without moral taint, there were no seeds of decay from which death 
    could reap its harvest. Under no sentence of dissolution, death had no power 
    to claim Him as its victim. As pure as our first parents before the fall, 
    like them in their original state of holiness, He was naturally deathless 
    and immortal. Had He not, by an act of the most stupendous grace, taken upon 
    Him the curse and sin of His Church, thereby making Himself responsible to 
    Divine justice for the utmost payment of her debt, the &quot;bitterness of death&quot; 
    had never touched His lips. But even then His death was voluntary. His 
    relinquishment of life was His own act and deed. The Jew who hunted Him to 
    the cross, and the Roman by whose hands He died, were but the actors in the 
    awful tragedy. The &quot;king of terrors&quot; wrenched not His spirit from Him. Death 
    waited the permission of Essential Life before he winged the fatal dart. 
    &quot;Jesus yielded up the spirit,&quot; literally, made a surrender, or let go His 
    spirit. Thus violent though it was, and responsible for the crime as were 
    its agents, the death of Jesus was yet voluntary. &quot;I lay down lay life,&quot; are 
    His expressive words. <br>
    <br>
    The control and power of Christ over bodily disease form one of the most 
    instructive and tender pages of His history when upon earth. We can but 
    briefly refer the reader to a few of the different traits of the Divine 
    Physician's grace, as illustrated by the various cures which He effected. 
    His promptness in healing the nobleman's son, John 4:43--54. His unsolicited 
    cure of the sick man at the pool of Bethesda, and the man with a withered 
    hand, John 5:1--9; Mark 3:1--6. The humility and delicacy with which He heals 
    the centurion's servant, Matt. 8:5--13. The tenderness with which He restored 
    the widow's son, Luke 7:11--17. The simplicity with which He recovered the 
    man born blind, John 9:1--7. The gentle touch with which He cured the man, 
    sick of the dropsy, Luke 14:1--6. The natural and spiritual healing of the 
    paralytic, Luke 5:17--28. The resistless compassion with which He cured the 
    daughter of the Syrophenician woman, Mark 8:24--3O. The wisdom and the 
    authority with which He healed the lunatic child, Luke 9:37--43. The power 
    with which He ejected the demons from the man, permitting their entrance 
    into the swine, Matthew 8:28--34. Truly the name of our Divine Physician is 
    &quot;Wonderful!&quot; All this skill and power and feeling He still possesses; and in 
    their exercise, in His present dealings with His suffering saints, is He 
    glorified. <br>
    <br>
    <br>
