    APRIL 28.<br>
    <br>
    O God, you know my foolishness; and my sins are not hid from you. Draw near 
    unto my soul, and redeem it: deliver me because of mine enemies. You have 
    known my reproach, and my shame, and my dishonor: mine adversaries are all 
    before you. Psalm 69:5, 18, 19. <br>
    <br>
    SATAN, we know, is the great accuser of the saints. And yet how insensible 
    are we of the great power which he still exerts over the people rescued 
    forever from his grasp. It was Satan who stood up to persuade David to 
    number Israel; it was Satan who would have prompted God to slay Job; and it 
    was Satan who stood at the right hand of Joshua, to condemn his soul. Thus 
    is He ever ready to assert his charge against the people of God. Not less 
    malignant is the world. Infidel in its principles, God-hating in its spirit, 
    and Christ-rejecting in its whole conduct, it is no marvel that it should be 
    the antagonist and the accuser of the saints. Sitting in judgment upon 
    actions, the nature of which it cannot understand--interpreting motives, the 
    character of which it cannot decide--ingeniously contriving and zealously 
    propagating reports of evil--ever ready to defame and to detract--all who live 
    godly in Christ Jesus must expect no mercy at its hand. Nor Satan and the 
    world only. How often, as the history of holy Job testifies, have the saints 
    been found the accusers of the saints (and with the deepest humiliation be 
    it written), with an uncharitableness and censoriousness which might have 
    kindled the world's cheek with the blush of shame. Thus does the Church 
    herself testify, &quot;My mother's children were angry with me.&quot; &quot;The watchmen 
    that went about the city found me; then smote me, they wounded me: the 
    keepers of the wall took away my veil from me.&quot; And from whom did our 
    blessed Lord receive His deepest wounds? Were they not from those who ranked 
    among His friends and followers. <br>
    <br>
    But what so keen and so bitter as self-reproach? Accusations proceeding from 
    others are often most unfounded and unjust. We have felt at the time the 
    secret and pleasing consciousness that we &quot;suffer wrongfully.&quot; The shaft 
    flies, but the arrow falls not more pointless and powerless than it. But far 
    different is the accusation which the true believer brings against himself. 
    Seeing sin where others see it not--conscious of its existence and its 
    perpetual working, where the saints applaud, and even the world admires--he 
    lays his hand upon his heart, his mouth in the dust, and exclaims, &quot;I am 
    vile! I abhor myself!&quot; Ah! no reproaches are like those which an honest, 
    sincere child of God charges upon himself. No accusation so true, no reproof 
    so keen, no reproaches so bitter. Happy are they who deal much in 
    self-condemnation. If we judged ourselves more, we should judge others less; 
    and if we condemned ourselves more, we should be less condemned. <br>
    <br>
    But what a privilege in all times of accusation, come from what quarter it 
    may, to be alone with Jesus! With Him, when we know the charge to be untrue, 
    to appeal to Him as an all-seeing, heart-searching, and righteous Judge, and 
    say, &quot;Lord, You know my principles, my spirit, my motives, my aim, and that 
    with honesty, purity, and singleness, I have sought to walk before You.&quot; Oh 
    it is a solace, the preciousness of which the throbbing heart may feel, but 
    the most eloquent pen cannot describe. And when the accusation is just, and 
    the believer feels, &quot;Vile as I am in the eyes of others, yet more vile am I 
    in my own eyes;&quot; yet even then to be left alone with Jesus, self-reproved, 
    self-condemned, is to be thrown upon the compassion of Him, &quot;very great are 
    whose mercies.&quot; Alone with Him, not a reproving glance darts from His eye, 
    nor an upbraiding word falls from His lips. All is mercy, all is tenderness, 
    all is love. There before Him the self-condemned may stand and confess; at 
    His feet the penitent may fall and weep, and find, alone with Jesus, His arm 
    a shield, and His bosom an asylum, within which his bleeding, panting heart 
    may find safety and repose. <br>
    <br>
    <br>
