    AUGUST 3.<br>
    <br>
    &quot;And many other signs truly did Jesus in the presence of his disciples, 
    which are not written in this book: but these are written, that you might 
    believe that Jesus is the Christ, the Son of God; and that believing you 
    might have life through his name.&quot; John 20:30, 31<br>
    <br>
    All the value and efficacy of the atoning blood is derived solely and 
    entirely from the dignity of the person who sheds it. If Christ do not be 
    absolutely and truly what the word of God declares, and what He Himself 
    professes to be, the true God, then, as it regards the great purpose for 
    which His atonement was made, namely, the satisfaction of Divine Justice, in 
    a full and entire sacrifice for sin, it were utterly valueless. We feel the 
    vast and solemn importance of this point; it is of the deepest moment--it is 
    the key-stone of the arch, sustaining and holding together every part of the 
    mighty fabric. Our examination of the claims of Christ to proper Deity 
    cannot be too close; we cannot too rigidly scrutinize the truth of His 
    Godhead; Jesus Himself challenges investigation. When personally upon earth, 
    carrying forward the great work of redemption, on all occasions, and by all 
    means, He announced and proved His Deity. Thus was He used to declare it--&quot;I 
    and my Father are one.&quot; &quot;Verily, verily, I say unto you, before Abraham was, 
    I AM.&quot; &quot;I come forth from the Father, and am come into the world; again, I 
    leave the world and go to the Father.&quot; Thus was He used to confirm it--&quot;I 
    have greater witness than that of John; for the works which the Father has 
    given me to finish, the same works that I do, bear witness of me that the 
    Father has sent me.&quot; &quot;If I do not the works of my Father, believe me not; 
    but if I do, though you believe not me, believe the works; that you may know 
    and believe that the Father is in me, and I in Him.&quot; Our blessed Lord saw 
    and felt the importance of a full belief in the doctrine of His Godhead. If 
    the foundation of our faith were not laid deep and broad in this, He well 
    knew that no structure, however splendid in its external form, could survive 
    the storm that will eventually sweep away every lying refuge. And what, to 
    the believing soul, is more animating than the full unwavering conviction of 
    the fact, that He who bore our sins in His own body on the tree was God in 
    our nature? that He who became our surety and substitute was Jehovah 
    Himself--&quot;God manifest in the flesh?&quot; that, as God, He became incarnate--as 
    God, He obeyed, and as God-man, He suffered the penalty? What deep views 
    does this fact give of sin! what exalted views of sin's atonement! Pray, 
    dear reader, that the blessed and eternal Spirit may build you up in the 
    belief of this truth. It is a truth on which we can live, and on no other 
    can we die. That Satan should often suggest suspicions to the mind 
    respecting the veracity of this doctrine we can easily imagine. That a dear 
    saint of God should at times find his faith wavering in its attempts to 
    grasp this wondrous fact, &quot;the incarnate mystery,&quot; we marvel not. It is the 
    very basis of his hope; is it surprising that Satan should strive to 
    overturn it? Satan's great controversy is with Christ. Christ came to 
    overthrow his kingdom, and He did overthrow it. Christ came to vanquish him, 
    and He triumphed. This signal and total defeat Satan will never forget. To 
    regain his kingdom he cannot. To recover what he has lost he knows to be 
    impossible. Therefore his shafts are leveled against Christ's members; and 
    the doctrine, to them most essential and precious--the doctrine of Christ's 
    Godhead--is the doctrine most frequently and severely assailed. Let no 
    believer sink in despondency under this severe temptation. Let him look 
    afresh to the cross, afresh to the atoning blood, and faith in Him, whose 
    word stilled the angry waves of the Galilean lake, and whose look prostrated 
    to the ground the soldiers sent to His arrest, will give Him the victory.<br>
    <br>
    <br>
