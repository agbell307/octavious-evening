    AUGUST 5.<br>
    <br>
    &quot;Knowing that Christ being raised from the dead dies no more; death has no 
    more dominion over him.&quot; Romans 6:9<br>
    <br>
    The resurrection of Christ was the consummation of His glorious victory. 
    Until this moment, the Redeemer had all the appearance of one vanquished in 
    the great fight. He was left slain upon the battle-field. Indeed it would 
    appear that He had really endured a momentary defeat. He was now under the 
    dominion of death; and as death was the consequence and penalty of sin, so 
    long as He was subject to its power, He still lay beneath the sins of His 
    people. Cancelled although they were by the blood He had just shed, the 
    great evidence of their remission did not and could not transpire until the 
    resurrection had passed. What gloom now enshrouded the church of God! The 
    Sun of Righteousness was setting in darkness and in blood; and with it was 
    descending into the tomb, the hopes of patriarchs and prophets, of seers and 
    apostles. The &quot;king of terrors&quot; had laid low his illustrious victim; and the 
    cold earth had closed upon His sacred body, mangled and lifeless. Oh, what a 
    victory did hell and sin, death and the grave, now seem to have achieved! 
    But the &quot;triumphing of the wicked is short.&quot; In three days the tomb, at the 
    mighty fiat of Jehovah, unveiled its bosom, and yielded back its Creator and 
    Lord. The Sun of Righteousness ascended again in cloudless glory and 
    peerless majesty, to set no more forever. The church of God, now &quot;begotten 
    again unto a lively hope by the resurrection of Jesus Christ from the dead,&quot; 
    arose from the dust, and put on her beautiful garments. Now was the scene 
    changed. His enemies, no longer wearing even the semblance of victory, were 
    overthrown and vanquished. Hell was disappointed, and its gates forever 
    closed against the redeemed. Sin was thrown to an infinite distance, and 
    &quot;death had no more dominion over him, God having loosed its pains, because 
    it was not possible that He should be holden of it.&quot; He rose a mighty and an 
    illustrious Conqueror. And all this conquest, let it not be forgotten, was 
    achieved in behalf of a chosen and a beloved people. It was our battle that 
    He fought, it was our victory that He won. Therefore, called though we are 
    to &quot;wrestle against principalities and against powers,&quot; and exhorted though 
    we are to &quot;take unto us the whole armor of God,&quot; we are yet confronted with 
    enemies already vanquished. It would seem as though we were summoned, not so 
    much to go out upon the field of battle, as upon the field of conquest; not 
    so much to combat with the foe, as to gather up the spoils of victory. For 
    what is every successful conflict with our spiritual adversaries--what is 
    every corruption mortified--what is every temptation resisted--what is every 
    sin overcome--but a showing forth the great victory already won by the 
    Captain of our salvation? Every triumph of the Holy Spirit in the heart of a 
    regenerate man is a display of the triumph of Him who, in hanging on the 
    cross, and in rising from the grave, &quot;spoiled principalities and powers, and 
    made a show of them openly, triumphing over them in it.&quot; <br>
    <br>
    <br>
