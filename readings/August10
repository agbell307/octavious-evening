    AUGUST 10.<br>
    <br>
    &quot;Look to yourselves, that we lose not those things which we have wrought, 
    but that we receive a reward. Whoever transgresses, and abides not in the 
    doctrine of Christ, has not God. He that abides in the doctrine of Christ, 
    he has both the Father and the Son.&quot; 2 John 8, 9<br>
    <br>
    Dear reader, in whose righteousness do you at this moment stand? Is it all 
    profession merely? Startle not at the question--turn not from it; it is for 
    your life we ask it. Do you wonder that such a scrutiny into the ground of 
    your hope should be made? Are you astonished at the solemn fact implied in 
    this question? Do not be so. Many have lived in the outward profession--have 
    put on Christ in the external garb--have talked well of Him--have been 
    baptized in His name--given liberally for His cause, and, after all, have 
    gone into eternity holding up the empty lamp! Oh, marvel not, then, that we 
    repeat the question--in whose righteousness do you at this moment stand? Mere 
    profession will not save your soul; your being found mingling among the wise 
    virgins will not secure you an admittance with them into heaven; your 
    talking respectfully of Jesus will avail you nothing; your church 
    membership, your liberality, your spotless morality, your regular attendance 
    on the sanctuary, all, all are in vain, without the justifying righteousness 
    of the God-man upon you. What do you know of the broken heart and the 
    contrite spirit? What do you know of the healing blood of Jesus? What do you 
    know of a sense of pardon and acceptance? What do you know of the witness of 
    the Spirit? What do you know of a humble, low, abasing view of yourself? 
    What do you know of a holy and a close walk with God? What do you know of 
    communion and fellowship with the Father and His dear Son? In a word, what 
    do you know of yourself as a helpless, ruined sinner; and of Jesus, as a 
    rich, able, and present Savior? Ponder these solemn questions. The hand that 
    pens them trembles with awe as it traces this page. This is a day of great 
    profession--a day of great ingathering into the church--a day when much chaff 
    must necessarily be gathered with the wheat. It solemnly behooves, then, 
    each professing member of Christ's church, of every name and denomination, 
    narrowly to scrutinize his motives, deeply to prove his heart, and closely 
    and habitually to examine the foundation on which he is building for 
    eternity. Thus shall he walk, if he be an adopted child, in the sweet and 
    holy realization of his pardon and acceptance; thus shall he experience the 
    blessedness of &quot;the man whose transgression is forgiven, whose sin is 
    covered;&quot; and thus, too, shall he constantly be &quot;a vessel unto honor, 
    sanctified, and meet for the Master's use, and prepared unto every good 
    work.&quot;<br>
    <br>
    <br>
