    DECEMBER 3.<br>
    <br>
    &quot;And the Lord direct your hearts into the love of God.&quot; 2 Thessalonians 3:5<br>
    <br>
    Love to God is the governing motive of the spiritual mind. All desire of 
    human admiration and applause pales before this high and holy principle of 
    the soul. Its religion, its devotion, its zeal, its toils, its sacrifices 
    spring from love. Love prompts, love strengthens, love sweetens, love 
    sanctifies all. This it is that expels from the heart the rival and false 
    claimant of its affections, and welcomes and enthrones the true. It may, at 
    times, like the pulse of the natural life, beat languidly; yet, unlike that 
    pulse, it never ceases entirely to beat. The love of God in the soul never 
    expires. Fed from the source from where it emanates, the holy fire, dim and 
    dying as it may appear at times, never goes out. <br>
    <br>
    Have you this evidence of the spiritual mind, my reader? Does the love of 
    Christ constrain you? It is the first and the chief grace of the Spirit--do 
    you possess it? &quot;Now abides faith, hope, and love; but the greatest of these 
    is love.&quot; It is the main-spring, the motive power, of the spiritual 
    mechanism of the soul; all its wheels revolve, and all its movements are 
    governed, by it. Is this the pure motive that actuates you in what you do 
    for God? Or, do there enter into your service and your sacrifice anything of 
    self-seeking, of thirst for human approbation, of desire to make a fair show 
    in the flesh, of aiming to make religion subserve your temporal interests? 
    Oh, search your hearts, and see; sift your motives, and ascertain! Love to 
    God--pure, unmixed, simple love--is the attribute of the spiritual mind; and, 
    in proportion to the intensity of the power of love as a motive, will be the 
    elevated tone of your spirituality. Nor need there be any lack of this 
    motive power. &quot;God is love,&quot; and He is prepared to supply it to the mind's 
    utmost capacity. We are straitened in ourselves, not in Him. The ocean on 
    whose margin we doubtingly, timidly stand is infinite, boundless, 
    fathomless. The Lord is willing to direct our hearts into its depths, but we 
    hesitate and draw back, awed by its infinite vastness, or stumbling at its 
    perfect freedom. But to a high standard of heavenly-mindedness, we must have 
    more of the love of God shed abroad in our hearts by the Holy Spirit, which 
    He has given unto us. We must love Christ more.<br>
    <br>
    <br>
