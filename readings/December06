    DECEMBER 6.<br>
    <br>
    &quot;A new commandment I give unto you, That you love one another; as I have 
    loved you, that you also love one another. By this shall all men know that 
    you are my disciples, if you have love one to another.&quot; John 13:34, 35<br>
    <br>
    There is one test--a gentle, sweet, and holy test--by which the most timid and 
    doubting child of God may decide the genuineness of his Christian 
    character--the evidence to which we allude is, love to the saints. The 
    apostle John presents this as a true test. He does not say, as he in truth 
    might have said, &quot;We know that we have passed from death unto life, because 
    we love God;&quot; but placing the reality of this wondrous translation upon a 
    lower evidence, the Holy Spirit, by the inspired writer, descends to the 
    weakest exhibition of the grace which his own power had wrought, when he 
    says, &quot;We know that we have passed from death unto life, because we love the 
    brethren.&quot; Thus so costly in God's eye would appear this heaven-born, 
    heaven-like grace, that even the faint and imperfect manifestation of it by 
    one saint to another, shall constitute a valid evidence of his relation to 
    God, and of his heirship to life eternal. <br>
    <br>
    Our blessed Lord, who is beautifully said to have been an incarnation of 
    love, places the evidence of Christian discipleship on precisely the same 
    ground; &quot;By this shall all men know that you are my disciples, if you have 
    love one to another.&quot; He might justly have concentrated all their affection 
    upon Himself, and thus have made their sole and supreme attachment to Him 
    the only test of their discipleship. But no! In the exercise of that 
    boundless benevolence which was never happy but as it was planning and 
    promoting the happiness of others, He bids them &quot;love one another;&quot; and 
    condescends to accept of this as evidencing to the world their oneness and 
    love to Himself.<br>
    <br>
    This affection, let it be remarked, transcends all similar emotions embraced 
    under the same general term. There is a natural affection, a humane 
    affection, and a denominational affection, which often binds in the sweetest 
    and closest union those who are of the same family, or of the same 
    congregation; or who assimilate in mind, in temper, in taste, or in 
    circumstance. But the affection of which we now speak is of a higher order 
    than this. We can find no parallel to it; not even in the pure, benevolent 
    bosoms of angels, until, passing through the ranks of all created 
    intelligences, we rise to God Himself. There, and there alone, we meet the 
    counterpart of Christian love. Believer, the love for which we plead is love 
    to the brethren--love to them as brethren. The church of God is one family, 
    of which Christ is the Elder Brother, and &quot;all are members one of another.&quot; 
    It is bound by a moral tie the most spiritual, it bears a family likeness 
    the most perfect, and it has a common interest in one hope the most sublime. 
    No climate, nor color, nor sect, affects the relationship. If you meet one 
    from the opposite hemisphere of the globe, having the image of Christ, 
    manifesting the fruits of the Spirit; who, in his walk and conversation, is 
    aiming to cultivate the heavenly dispositions and holy habits of the gospel, 
    and who is identifying himself with the cause of God and of truth--and you 
    meet with a member of the one family, a brother in the Lord, one who calls 
    your Father his Father, your Lord his Lord; and one, too, who has a higher 
    claim upon your affection and your sympathy than the closest and the 
    tenderest natural relation that life can command.<br>
    <br>
    <br>
