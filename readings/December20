    DECEMBER 20.<br>
    <br>
    &quot;And to Jesus the mediator of the new covenant, and to the blood of 
    sprinkling, that speaks better things than that of Abel.&quot; Hebrews 12:24<br>
    <br>
    The subject lifts us to the very porch, and within the porch of heaven. And 
    what is the great truth which it presents to our view there?--the prevalency 
    of the life-blood of Jesus within the veil. The moment the ransomed and 
    released soul enters glory, the first object that arrests its attention and 
    fixes its eye is--the interceding Savior. Faith, anticipating the glorious 
    spectacle, sees Him now pleading the blood on behalf of each member of His 
    church upon earth. &quot;By His own blood He entered in once into the holy place, 
    having obtained eternal redemption for us.&quot; &quot;For Christ is not entered into 
    the holy places made with hands, which are the figures of the true; but into 
    heaven itself, now to appear in the presence of God for us.&quot; There is blood 
    in heaven! the blood of the Incarnate God! And because it pleads and prays, 
    argues and intercedes, the voice of every sin is hushed, every accusation of 
    Satan is met, every daily transgression is forgiven, every temptation of the 
    adversary is repelled, every evil is averted, every want is supplied, and 
    the present sanctification and the final glorification of the saints are 
    secured. &quot;Who shall lay anything to the charge of God's elect? It is God 
    that justifies. Who is he that condemns? It is Christ that died, yes rather, 
    that is risen again, who is even at the right hand of God, who also makes 
    intercession for us.&quot; Draw near, you Joshuas, accused by Satan! Approach, 
    you Peters, whose faith is sifted! Come, you tried and disconsolate! The 
    mediatorial Angel, the pleading Advocate, the Interceding High Priest, is 
    passed into the heavens, and appears before the throne for you. If the 
    principle of the new life in your soul has decayed, if your grace has 
    declined, if you have &quot;left your first love,&quot; there is vitality in the 
    interceding blood of Jesus, and it prays for your revival. If sin condemns, 
    and danger threatens, if temptation assails, and affliction wounds, there is 
    living power in the pleading blood of Immanuel, and it procures pardon, 
    protection, and comfort.<br>
    <br>
    Nor let us overlook the sanctifying tendency of the pleading blood. &quot;These 
    things I write unto you, that you sin not.&quot; The intercession of Jesus is 
    holy, and for holiness. The altar of incense is of &quot;pure gold.&quot; The advocacy 
    of Christ is not for sin, but for sinners. He prays not for the continuance 
    of sin, but for the putting away of sin. &quot;The righteous Lord loves 
    righteousness.&quot; If sensible of our sin--if mourning over our sin--if loathing 
    and turning from our sin--we come to God through Christ, then &quot;we have an 
    advocate with the Father, Jesus Christ the Righteous.&quot; The odor-breathing 
    censer is in His hand--the fragrant cloud goes up--the mercy-seat is 
    enveloped--the Father smiles--and all once more is peace! Then, &quot;I will arise 
    and go to my Father, and will say unto him, Father, I have sinned against 
    heaven, and before you, and am no more worthy to be called your son.&quot;<br>
    <br>
    <br>
