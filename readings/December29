    DECEMBER 29.<br>
    <br>
    &quot;Why seeing we also are compassed about with so great a cloud of witnesses, 
    let us lay aside every weight, and the sin which does so easily beset us, 
    and let us run with patience the race that is set before us.&quot; Hebrews 12:1<br>
    <br>
    The Bible is rich in its illustrations of this principle of the Divine 
    government, that all that occurs in the Lord's guidance of His people 
    conspires for, and works out, and results in, their highest happiness, their 
    greatest good. Take, for example, the case of Jacob. Heavy and lowering was 
    the cloud now settling upon his tabernacle. Severe was the test, and fearful 
    the trembling of his faith. His feet were almost gone. The sad recollections 
    of his bereavement still hovered like clinging shadows around his memory; 
    gaunt famine stared him in the face; and a messenger with tidings of yet 
    heavier woe lingered upon the threshold of his door. And when those tidings 
    broke upon his ear, how touching the expression of his grief!--&quot;Me have you 
    bereaved of my children: Joseph is not, and Simeon is not, and you will take 
    Benjamin away: all these things are against me.&quot; But lo! the circumstances 
    which to the dim eye of his faith wore a hue so somber, and an aspect so 
    alarming, were at that moment developing and perfecting the events which 
    were to smooth his passage to the grave, and shed around the evening of his 
    life the halo of a glorious and a cloudless sunset. All things were working 
    together for his good!<br>
    <br>
    Joseph, too, reviewing the past of his chequered and mysterious history, 
    arrives at the same conclusion, and confirms the same truth. Seeking to 
    tranquilize his self-condemning brothers, he says, &quot;But as for you, you 
    thought evil against me; but God meant it unto good, to bring to pass, as it 
    is this day, to save much people alive.&quot; The envy of his brethren, his being 
    sold as a slave, his imprisonment, were all working out God's purpose and 
    plan of wisdom and love. And yet, who could have foreseen and predicted, 
    that from those untoward events, the exaltation, power, and wealth of Joseph 
    would spring? Yet all things were working together for good.<br>
    <br>
    Thus is it, too, in the history of the Lord's loving corrections. They are 
    all the unfoldings of a design, parts of a perfect whole. From these 
    dealings, sometimes so heart-crushing, what signal blessings flow! &quot;You have 
    chastised me, and I was chastised.&quot; And what was the result? It awoke from 
    Ephraim this precious acknowledgment and prayer--&quot;Surely after that I was 
    turned, I repented; and after that I was instructed, I smote upon my thigh: 
    I was ashamed, yes, even confounded, because I did bear the reproach of my 
    youth.&quot; Oh, who can compute the good, the real, the permanent good, that 
    results from the trying dispensations of God?--from the corrections of a 
    Father's love? The things that appear to militate against the believer, 
    unfolding their heaven-sent mission, turn out rather for the furtherance of 
    his best welfare and his highest interest.<br>
    <br>
    <br>
