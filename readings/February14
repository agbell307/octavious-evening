    FEBRUARY 14. <br>
    <br>
    For every high priest taken from among men is ordained for men in things 
    pertaining to God, that he may offer both gifts and sacrifices for sins: Who 
    can have compassion on the ignorant, and on those who are out of the way; 
    for that he himself also is compassed with infirmity. Hebrews 5:1-2<br>
    <br>
    OVERLOOK not the fitness of the Lord Jesus to meet all the infirmities of 
    His people. There are two touching and expressive passages bearing on this 
    point. &quot;Himself took our infirmities, and bare our sicknesses.&quot; Wondrous 
    view of the Incarnate God! That very infirmity, Christian reader, which now 
    bogs you to the earth, by reason of which you can in no wise lift up 
    yourself-- your Savior bore. Is it sin? is it sorrow? is it sickness? is it 
    want? It bowed Him to the dust, and brought the crimson drops to His brow. 
    And is this no consolation? Does it not make your infirmity even pleasant, 
    to remember that Jesus once bore it, and in sympathy bears it still? The 
    other passage is--&quot;We have not an high priest which cannot be touched with 
    the feeling of our infirmities.&quot; Touched with my infirmity! What a thought! 
    I reveal my grief to my friend, I discern the emotions of his soul. I mark 
    the trembling lip, the sympathizing look, the moistened eye--my friend is 
    touched with my sorrow. But what is this sympathy--tender, soothing, grateful 
    as it is--to the sympathy with which the great High Priest in heaven enters 
    into my case, is moved with my grief, is touched with the feeling of my 
    infirmity? <br>
    <br>
    Let us learn more tenderly to sympathize with the infirmities of our 
    brethren. &quot;We that are strong ought to bear the infirmities of the weak, and 
    not to please ourselves.&quot; Oh for more of this primitive Christianity! The 
    infirmity of a Christian brother should by a heartfelt sympathy become in a 
    measure our own. We ought to bear it. The rule of our conduct towards him 
    should be the rule of our conduct towards our own selves. Who would feel 
    bound or disposed to travel from house to house, proclaiming with trumpet 
    tongue, and with evident satisfaction, his own weaknesses, failings, and 
    infirmities? To God we may confess them, but no divine precept enjoins their 
    confession to man. We unveil them to His eye, and He kindly and graciously 
    veils them from all human eyes. Be this our spirit, and our conduct, towards 
    a weak and erring brother. Let us rather part with our right hand than 
    publish his infirmity to others, and thus wound the Head by an unkind and 
    unholy exposure of the faults and frailties of a member of His body; and by 
    so doing cause the enemies of Christ to blaspheme that worthy name by which 
    we are called. <br>
    <br>
    Honor and glorify the Spirit, who thus so graciously and so kindly 
    sympathizes with our infirmities. Pay to Him divine worship, yield to Him 
    divine homage; and let your unreserved obedience to His commands, your 
    jealous regard for His honor, and your faithful hearkening to the gentle 
    accents of His &quot;still, small voice,&quot; manifest how deeply sensible you are of 
    His love, His grace, and His faithfulness, in sympathizing with your 
    sorrows, in supplying your need, and in making your burdens and infirmities 
    all and entirely His own. <br>
    <br>
    Nor let us forget that, so condescending is Jesus, He regards Himself as 
    honored by the confidence which reposes our sorrows upon His heart. The 
    infirmity which we bring to His grace, and the sin which we bring to His 
    atonement, and the trials which we bring to His sympathy, unfold Jesus as He 
    is--and so He is glorified. Consequently, the oftener we come, the more 
    welcome we are, and the more precious does Jesus become. <br>
    <br>
    <br>
