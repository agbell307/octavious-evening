    FEBRUARY 24.<br>
    <br>
    When he had by himself purged our sins, sat down on the right hand of the 
    Majesty on high. Hebrews 1:3<br>
    <br>
    WHAT a blessed declaration is this!--the words are inexpressibly sweet. 
    Having finished His work, having made an end of sin, having brought in an 
    everlasting righteousness, having risen from the grave, having ascended up 
    on high, Christ has sat down at the right hand of God, reposing in the full 
    satisfaction, glory, and expectancy of His redeeming work. And for what 
    object is He there seated? Why is He thus presented to the eye of faith? 
    That the Church of God might have visibly and constantly before its view a 
    risen, living Christ. Oh how constantly is the Lord teaching us that there 
    is but one Being who can meet our case, and but one Object on which our 
    soul's affections ought to be supremely placed--even a risen Savior. We have 
    temptations various; trials the world know nothing of, crosses which those 
    who know and love us the most, never suspect; for often the heart's acutest 
    sorrow is the least discoverable upon the surface. <br>
    <br>
    But here is our great mercy--Christ is alive. What if we are unknown, tried, 
    tempted, and sad; we yet have a risen Savior to go to, who, as Rutherford 
    says, &quot;sighs when I sigh, mourns when I mourn, and when I look up He 
    rejoices.&quot; How can I want for sympathy, when I have a risen Christ? how can 
    I feel alone and sad, when I have the society and the soothing of a living 
    and an ever present Jesus--a Jesus who loves me, who knows all my 
    circumstances, all my feelings, and has His finger upon my every pulse--who 
    sees all my tears, hears all my sighs, and records all my thoughts--who, go 
    to Him when I will, and with what I will, will never say to me no, nor bid 
    me depart unblest--who is risen, exalted, and is set down at the right hand 
    of His Father and my Father, His God and my God, to administer to me all the 
    blessings of the everlasting covenant, and to mete out, as I need them, all 
    the riches of His grace and the supplies of His salvation? Why then should I 
    despond at any circumstance, why despair at any emergency, or sink beneath 
    any trial, when I have a risen, a living Christ to go to? Oh the amazing 
    power of the Lord's resurrection! Oh the preciousness of the fruit that 
    springs from it! Communion with our heavenly Father, near walking with God, 
    a life of faith in Christ, living on high--living not only on Christ's 
    fullness, but on Christ himself; not only on what He has, but on what He is, 
    in His godhead, in His humanity, in the tenderness of His heart, as well as 
    the fullness of His salvation; living in the blessed anticipation of glory, 
    and honor, and immortality; rising in the morning and saying, &quot;This day, and 
    every day, I would consecrate to my God;&quot;--these are some of the fadeless 
    flowers and precious fruits that grow around the grave of Jesus, when faith, 
    listening to the voice that issues from the vacant sepulcher--&quot;He is not 
    here, but is risen&quot;--looks up and beholds Him alive, &quot;seated at the right 
    hand of the Majesty on high.&quot; Then, oh then, it exclaims in a transport of 
    joy, &quot;Whom have I in heaven but you? and there is none upon earth I desire 
    beside you,&quot; you risen, living, and glorious Redeemer! <br>
    &quot;Oh, there is nothing in yon bright sky, <br>
    Worthy this worthless heart to own; <br>
    On earth there's nothing; friends, creatures, fly; <br>
    I pant, my Lord, for You alone.&quot; <br>
    <br>
    <br>
