    FEBRUARY 28.<br>
    <br>
    An inheritance among all those who are sanctified. Acts 20:32. <br>
    <br>
    UNDER the figure of an inheritance, heaven is here presented to the mind. 
    Nor is this the only passage in which the same similitude occurs. In the 
    first chapter of the Ephesians, and the eleventh verse, we read--&quot;In whom 
    also we have obtained an inheritance, being predestinated according to the 
    purpose of Him who works all things after the counsel of His own will:&quot; and 
    in the thirteenth verse of the same chapter, it will be observed, we have a 
    pledge or an earnest of this inheritance--&quot;In whom, also, after you believed, 
    you were sealed with that Holy Spirit of promise, which is the earnest of 
    our inheritance, until the redemption of the purchased possession, to the 
    praise of His glory.&quot; And if it be inquired what the saints of God do thus 
    inherit, the answer will be found in the twenty-first chapter of the 
    Revelation by John, and the seventh verse: &quot;He that overcomes shall inherit 
    all things; and I will be his God, and he shall be my son.&quot; How vast, how 
    illimitable, then, the inheritance of the saints--inheriting &quot;all things&quot;! It 
    is a beautiful idea of heaven; it is a lovely picture, on which the eye of 
    faith delights to dwell. The earthly heir looks at his inheritance, surveys 
    it, walks through it, luxuriates amid its beauties, and anticipates its full 
    possession. The heir of glory has his inheritance too; it is heaven. He 
    looks to it, he longs for it; and soon the Savior will come in personal 
    glory, and institute him into its full and eternal possession. But whose is 
    this inheritance? It is the inheritance of the Lord's holy ones, of every 
    nation and from every fold. They form the whole election of grace--the 
    chosen, ransomed, called people of God, be their outward name among men what 
    it may--all who are sanctified by God the Father--all who have been washed in 
    the blood of the Lamb--all who are renewed by the Holy Spirit--all who have 
    &quot;the white stone&quot; and the &quot;new name&quot; in that white stone--all who are living 
    holy, godly lives, in whom dwells the Holy Spirit--and by whose grace the 
    Lord is, day by day, step by step, carrying on that blessed kingdom of grace 
    in their hearts, which will soon fit them for the full possession of eternal 
    glory. <br>
    <br>
    And what is the great end of all God's dealings with His people? For what 
    purpose is the Lord's furnace in Zion, and His fire in Jerusalem? It is to 
    purify, and sanctify, and fit the believer for &quot;an inheritance among all 
    those who are sanctified.&quot; All your heaven-blessed trials, all your 
    sanctified temptations, all the covenant transactions of God with you, 
    beloved, in the way of afflictive providences, are designed but to fit you 
    more thoroughly for your inheritance. In this point of view, who would not 
    welcome the severest chastisement? who would not drink willingly the 
    bitterest cup? who would not be willing to have the fetter unbound, the 
    chain snapped, the bond severed, that gives liberty to his struggling and 
    ascending spirit, and brings him in a state of holy fitness nearer and still 
    nearer to heaven? <br>
    <br>
    <br>
