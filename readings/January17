    JANUARY 17. <br>
    <br>
    And I will bring the blind by a way that they knew not; I will lead them in 
    paths that they have not known: I will make darkness light before them, and 
    crooked things straight. These things will I do unto them, and not forsake 
    them. Isaiah 42:16<br>
    <br>
    THESE words imply a concealment of much of the Lord's procedure with His 
    people. With regard to our heavenly Father, there can be nothing mysterious, 
    nothing inscrutable to Him. A profound and awful mystery Himself, yet to His 
    infinite mind there can be no darkness, no mystery at all. His whole plan--if 
    plan it may be called--is before Him. Our phraseology, when speaking of the 
    Divine procedure, would sometimes imply the opposite of this. We talk of 
    God's fore-knowledge, of His foresight, of His acquaintance with events yet 
    unborn; but there is, in truth, no such thing. There are no tenses with 
    God--no past--nor present--nor future. The idea of God's eternity, if perfectly 
    grasped, would annihilate in our minds all such humanizing of the Divine 
    Being. He is one ETERNAL NOW. All events, to the remotest period of time, 
    were as vivid and as present to the Divine mind from eternity, as when at 
    the moment they assumed a real existence and a palpable form. <br>
    <br>
    But all the mystery is with us, poor finite creatures of a day. And why, 
    even to us, is any portion of the Divine conduct thus a mystery? Not because 
    it is in itself so, but mainly and simply because we cannot see the whole as 
    God sees it. Could it pass before our eye, as from eternity it has before 
    His, a perfect and a complete whole, we should then cease to wonder, to 
    cavil, and repine. The infinite wisdom, purity, and goodness that originated 
    and gave a character, a form, and a coloring to all that God does, would 
    appear as luminous to our view as to His, and ceaseless adoration and praise 
    would be the grateful tribute of our loving hearts. Let us, then, lie low 
    before the Lord, and humble ourselves under His mysterious hand. &quot;The meek 
    will He guide in judgment, and the meek will He teach His way. All the paths 
    of the Lord are mercy and truth unto such as keep His covenant and His 
    testimonies.&quot; Thus writing the sentence of death upon our wisdom, our 
    sagacity, and our strength, Jesus--the lowly one--seeks to keep us from the 
    loftiness of our intellect and from the pride of our heart--prostrating us 
    low in the dust at His feet. Holy posture! blessed place! There, Lord, would 
    I lie; my trickling tears of penitence and of love falling upon those dear 
    feet that have never misled, but have always gone before, leading me by a 
    right way, the best way, to a city of rest. Wait, then, suffering believer, 
    the coming glory--yielding yourself to the guidance of your Savior, and 
    submitting yourself wholly to your Father's will. <br>
    <br>
