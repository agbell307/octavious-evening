    JANUARY 22.<br>
    <br>
    Now he which establishes us with you in Christ, and has anointed us, is God; 
    Who has also sealed us, and given the earnest of the Spirit in our hearts. 2 
    Cor. 1:21-22<br>
    <br>
    THE Holy Spirit renews, sanctifies, and inhabits the believer as a Divine 
    person. It is not the common light of nature, nor the ordinary teaching of 
    man, nor the moral suasion of truth, which has made him what he is--an 
    experimental CHRISTIAN: all his real grace, his true teaching, flows from 
    the Divine Spirit. His light is divine, his renewing is divine, his 
    sanctification is divine. There is more real value in one ray of the 
    Spirit's light, beaming in upon a man's soul, than in all the teaching which 
    books can ever impart! The Divine Spirit, loosing the seals of the written 
    Word, and unfolding to him the mysteries of the kingdom, the glories of 
    Christ's person, the perfection of Christ's work, the fullness of Christ's 
    grace, the revealed mind and will of God, has in it more wealth and glory 
    than all the teaching the schools ever imparted. How precious the grace of 
    the Holy Spirit, what tongue is sufficiently gifted to describe! How 
    precious is his indwelling--an ever-ascending, heaven-panting, God-thirsting, 
    Christ-desiring Spirit! How precious are all the revelations He makes of 
    Christ! How precious are the consolations He brings, the promises He seals, 
    the teachings He imparts, all the emotions He awakens, the breathings He 
    inspires, and the affections He creates! How precious are those graces in 
    the soul of which He is the Author--the faith that leads to a precious 
    Savior, the love that rises to a gracious God, and the holy affections which 
    flow forth to all the saints! <br>
    <br>
    But through what channel does this Divine anointing come? Only through the 
    union of the believer to Christ, the Anointed One. All the saving operations 
    of the Spirit upon the mind are connected with Jesus. If He convinces of 
    sin, it is to lead to the blood of Jesus; if He reveals the corruption of 
    the heart, it is to lead to the grace of Jesus; if He teaches the soul's 
    ignorance, it is to conduct it to the feet of Jesus: thus all His operations 
    in the soul are associated with Jesus. Now, in conducting this holy 
    anointing into the soul, He brings it through the channel of our union with 
    the Anointed Head. By making us one with Christ, He makes us partakers of 
    the anointing of Christ. And truly is the weakest, lowliest believer one 
    with this anointed Savior. His fitness, as the Anointed of God, to impart of 
    the plenitude of His anointing to all the members of his body, is a truth 
    clearly and beautifully set forth. Thus is He revealed as the Anointed Head 
    of the Church, the great High Priest of the royal priesthood: &quot;You loves 
    righteousness, and hate wickedness: therefore God, your God, has anointed 
    You with the oil of gladness above your fellows.&quot; &quot;The Spirit of the Lord 
    God is upon me; because the Lord has anointed me to preach good tidings unto 
    the meek.&quot; In the Acts of the Apostles a distinct reference is made to this 
    truth: &quot;how God anointed Jesus of Nazareth with the Holy Spirit and with 
    power.&quot; His human soul filled with the measureless influence of the Divine 
    Spirit, the fullness of the Godhead dwelling in Him bodily, He became the 
    true Aaron, of whose anointing all the priests were alike to partake. One, 
    then, with Jesus, through the channel of his union to the Head, the lowest 
    member is anointed with this Divine anointing. <br>
    <br>
    <br>
