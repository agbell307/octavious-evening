    JULY 9.<br>
    <br>
    &quot;He shall baptize you with the Holy Spirit.&quot; Mark 1:8<br>
    <br>
    &quot;Neither will I hide my face any more from them; for I have poured out my 
    Spirit upon the house of Israel, says the Lord God.&quot; Ezekiel 39:29<br>
    <br>
    In a more enlarged communication of the Holy Spirit's gracious influence 
    lies the grand source and secret of all true, spiritual, believing, 
    persevering, and prevailing prayer; it is the lack of this that is the cause 
    of the dullness, and formality, and reluctance, that so frequently mark the 
    exercise. The saints of God honor not sufficiently the Spirit in this 
    important part of His work; they too much lose sight of the truth, that of 
    all true prayer He is the Author and the Sustainer, and the consequence is, 
    and ever will be, self-sufficiency and cold formality in the discharge, and 
    ultimate neglect of the duty altogether. But let the promise be pleaded, &quot;I 
    will pour upon the house of David, and upon the inhabitants of Jerusalem, 
    the spirit of grace and of supplication;&quot; let the Holy Spirit be 
    acknowledged as the Author, and constantly sought as the Sustainer, of this 
    holy exercise; let the saint of God feel that he knows not what he should 
    pray for as he ought, that the Spirit itself makes intercession for us with 
    groanings which cannot be uttered, and that God knows the mind of the 
    Spirit, because He makes intercession for the saints according to His will; 
    and what an impulse will this give to prayer! what new life will it impart! 
    what mighty energy, what unction, and what power with God! Seek, then, with 
    all your blessings, this, the richest, and the pledge of all, the baptism of 
    the Spirit; rest not short of it. You are nothing as a professing man 
    without it; your religion is lifeless, your devotion is formal, your spirit 
    is unctionless; you have no moral power with God, or with man, apart from 
    the baptism of the Holy Spirit. Seek it, wrestle for it, agonize for it, as 
    transcendently more precious than every other mercy. Submerged in His 
    quickening and reviving influences, what a different Christian will you be! 
    How differently will you pray, how differently will you live, and how 
    differently will you die! Is the spirit of prayer languishing? is its 
    exercise becoming irksome? is closet-devotion abandoned? is the duty in any 
    form becoming a task? Oh, rouse you to the seeking of the baptism of the 
    Spirit! This alone will revive the true spirit of prayer within you, and 
    this will give to its exercise sweetness, pleasantness, and power. God has 
    promised the bestowment of the blessing, and He will never disappoint the 
    soul that seeks it.<br>
    <br>
    <br>
