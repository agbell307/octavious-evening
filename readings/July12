    JULY 12.<br>
    <br>
    &quot;Herein is love, not that we loved God, but that he loved us, and sent his 
    Son to be the propitiation for our sins.&quot; 1 John 4:10<br>
    <br>
    It is a self-evident truth, that as God only knows, so He only can reveal 
    His own love. It is a hidden love, veiled deep within the recesses of His 
    infinite heart; yes, it seems to compose His very essence, for, &quot;God is 
    love,&quot;--not merely lovely and loving, but love itself, essential love. Who, 
    then, can reveal it but Himself? How dim are the brightest views, and how 
    low the loftiest conceptions, of the love of God, as possessed by men of 
    mere natural and speculative knowledge of divine things! They read of God's 
    goodness, even in nature, with a half-closed eye, and spell it in providence 
    with a stammering tongue. Of His essential love--His redeeming love--of the 
    great and glorious manifestation of His love in Jesus, they know nothing. 
    The eyes of their understanding have not been opened; and &quot;God, who 
    commanded the light to shine out of darkness,&quot; has not as yet &quot;shined into 
    their hearts, to give the light of the knowledge of the glory of God in the 
    face of Jesus Christ.&quot; <br>
    <br>
    But God has declared His own love--Jesus is its glorious revelation. &quot;In this 
    was manifested the love of God toward us, because that God sent His only 
    begotten Son into the world, that we might live through Him.&quot; Oh, what an 
    infinite sea of love now broke in upon our guilty and rebellious world, 
    wafting in upon its rolling tide God's only begotten Son! That must have 
    been great love--love infinite, love unsearchable, love passing all 
    thought--which could constrain the Father to give Jesus to die for us, &quot;while 
    we were yet sinners.&quot; It is the great loss of the believer that faith eyes 
    with so dim a vision this amazing love of God in the gift of Jesus. We have 
    transactions so seldom and so unbelievingly with the cross, that we have 
    need perpetually to recur to the apostle's cheering words, written as if 
    kindly and condescendingly to meet this infirmity of our faith--&quot;He that 
    spared not His own Son, but delivered Him up for us all, how shall He not 
    with Him also freely give us all things!&quot; <br>
    <br>
    But, behold God's love! See how He has inscribed this glorious perfection of 
    His nature in letters of blood drawn from the heart of Jesus. His love was 
    so great, that nothing short of the surrender to the death of His beloved 
    Son could give an adequate expression of its immensity. &quot;For God so loved 
    the world, that He gave His only begotten Son.&quot; Here was the greatest 
    miracle of love--here was its most stupendous achievement--here its most 
    brilliant victory--and here its most costly and precious offering. Seeing us 
    fallen, obnoxious to the law's curse, exposed to its dreadful penalty, 
    guilty of innumerable sins, and deserving of as many deaths, yet how did it 
    yearn to save us! How did it heave, and pant, and strive, and pause not, 
    until it revealed a way infinitely safe for God and man; securing glory to 
    every Divine attribute in the highest degree, and happiness to the creature, 
    immense, unspeakable, and eternal.<br>
    <br>
    <br>
