    JULY 18.<br>
    <br>
    &quot;Whatever you shall ask in my name, that will I do, that the Father may be 
    glorified in the Son.&quot; John 14:13<br>
    <br>
    In the matter of prayer, ever cultivate and cherish a kindly, soothing view 
    of God in Christ. Without it, in this most solemn and holy of all 
    transactions, your mental conceptions of His nature will be vague, your 
    attempts to concentrate your thoughts on this one object will be baffled, 
    and the spiritual character of the engagement will lessen in tone and vigor. 
    But meeting God in Christ, with every perfection of His nature revealed and 
    blended, you may venture near, and in this posture, and through this medium, 
    may negotiate with Him the most momentous matters. You may reason, may 
    adduce your strong arguments, and throwing wide the door of the most hidden 
    chamber of your heart, may confess its deepest iniquity; you may place your 
    &quot;secret sins in the light of His countenance;&quot; God can still meet you in the 
    mildest luster of His love. Drawing near, placing your tremulous hand of 
    faith on the head of the atoning sacrifice, there is no sin that you may not 
    confess, no want that you may not make known, no mercy that you may not ask, 
    no blessing that you may not crave, for yourself, for others, for the whole 
    church. See! the atoning Lord is upon the mercy-seat, the golden censer 
    waves, the fragrant cloud of the much incense ascends, and with it are 
    &quot;offered the prayers of all saints upon the golden altar which is before the 
    throne.&quot; Jesus is in its midst--<br>
    <br>
    &quot;Looks like a Lamb that has been slain, <br>
    And wears His priesthood still.&quot; <br>
    <br>
    &quot;Having therefore, brethren, boldness to enter into the holiest by the blood 
    of Jesus, and having an High Priest over the house of God, let us draw 
    near.&quot; Open all your heart to God through Christ, who has opened all His 
    heart to you in Christ. Remember that to bring Himself in a position to 
    converse with you, as no angel could, in the matter that now burdens and 
    depresses you, He assumed your nature on earth, with that very sorrow and 
    infirmity affixed to it; took it back to glory, and at this moment appears 
    in it before the throne, your Advocate with the Father. Then hesitate not, 
    whatever be the nature of your petition, whatever the character of your 
    need, to &quot;make known your requests unto God.&quot; Coming by simple faith in the 
    name of Jesus, it cannot be that He should refuse you. With His eye of 
    justice ever on the blood, and His eye of complacency ever on His Son, 
    Himself loving you, too, with a love ineffably great, it would seem 
    impossible that you should meet with a denial. Yield your ear to the sweet 
    harmony of the Redeemer's voice, &quot;Verily, verily, I say unto you, Whatever 
    you shall ask the Father in my name, He will give it you. Hitherto have you 
    asked nothing in my name; ask, and you shall receive, that your joy may be 
    full.&quot;<br>
    <br>
    <br>
