    JUNE 11. <br>
    <br>
    &quot;Partakers of the heavenly calling.&quot; Heb. 3:1<br>
    <br>
    WHAT are some of the attributes of this calling? It is holy. &quot;Who has saved 
    us, and called us with an holy calling.&quot; They who are the subjects of this 
    call desire to be holy. Their direst evil is sin. It is, in their 
    experience, not a silken chain, but a galling fetter, beneath whose weight 
    they mourn, and from whose bondage they sigh to be delivered. It is a high 
    and heavenly calling. &quot;I press toward the mark for the prize of the high 
    calling of God in Christ Jesus.&quot; &quot;Partakers of the heavenly calling.&quot; How 
    does this calling elevate a man--his principles, his character, his aims, his 
    hopes! It is emphatically a &quot;high vocation.&quot; So heavenly is it, too, it 
    brings something of heaven into the soul. It imparts heavenly affections, 
    heavenly joys, and heavenly aspirations. It leads to heaven. Could he look 
    within the veil, each called saint would see a prepared mansion, a vacant 
    throne, a jeweled crown, a robe, and a palm, all ready for the wearing and 
    the waving, awaiting him in glory. Thus it is a call from heaven, and to 
    heaven. It is an irrevocable calling. &quot;The gifts and calling of God are 
    without repentance.&quot; God has never for a moment repented that He chose, nor 
    has the Savior repented that He redeemed, nor has the Spirit repented that 
    He called any of His people. Not all their wanderings, nor failures, nor 
    unfruitfulness have ever awakened one regret in the heart of God that He has 
    called them to be saints. &quot;I knew that You would deal very treacherously.&quot; 
    &quot;Then will I visit their transgression with the rod, and their iniquity with 
    stripes. Nevertheless my loving-kindness will I not utterly take from him; 
    nor suffer my faithfulness to fail.&quot; &quot;Faithful is He that calls you.&quot;<br>
    <br>
    Nor must we overlook the Divine sovereignty, which appears so illustrious in 
    this especial calling. All ground of human boasting is removed, and God has 
    secured to Himself, from eternity, the entire glory of His people's 
    salvation. So conspicuously appears the sovereignty of God in this effectual 
    calling, that all foundation of creature-glory is annihilated. And if it be 
    asked by the disputers of this truth, why one is called and another is 
    left?--why Jacob, and not Esau?--why David, and not Saul?--why Cornelius the 
    Gentile, and not Tertullus the Jew?--why the poor beggars in the highway, and 
    not the bidden guests? why the woman who washed with her tears the Savior's 
    feet, and not Simon, in whose house the grateful act was performed?--the 
    answer is, &quot;He will have mercy upon whom He will have mercy.&quot; To this 
    acquiescence in the sovereignty of the Divine will our Lord was brought, 
    when He beheld the mysteries of the Gospel veiled from the wise of this 
    world: &quot;I thank You, O Father, Lord of heaven and earth, that You have hid 
    these things from the wise and prudent, and have revealed them unto babes. 
    Even so, Father: for so it seemed good in Your sight.&quot; To this precious 
    truth let us bow; and if the efficacious grace of God has reached our 
    hearts, let us ascribe its discriminating choice to the sovereign pleasure 
    of that Divine and supreme will, which rules over the armies of heaven and 
    among the inhabitants of earth, and to which no creature dare say, &quot;What do 
    you?&quot;<br>
    <br>
    <br>
