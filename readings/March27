    MARCH 27. <br>
    <br>
    Who is even at the right hand of God, who also makes intercession for us. 
    Romans 8:34<br>
    <br>
    THE exaltation of Jesus in heaven is associated with the dearest interests 
    of His people on earth. Joseph was forgotten when Pharaoh lifted up the head 
    of the chief butler. But our Lord, amid the honors and splendors to which 
    God has highly exalted him, still remembers his brethren in bonds, and makes 
    intercession for them. How expressive is the type of our Lord's present 
    engagement on behalf of His people. &quot;And he (Aaron) shall take a censer full 
    of burning coals of fire from of the altar before the Lord, and his hands 
    full of sweet incense beaten small, and bring it within the veil: and he 
    shall put the incense upon the fire before the Lord, that the cloud of the 
    incense may cover the mercy-seat that is upon the testimony.&quot; The passing of 
    Aaron into the holy of holies was the shadowing forth of our Lord's entrance 
    into heaven. The blood sprinkled at the mercy-seat was the presentation of 
    the great Atonement within the veil. And the incense overshadowing with its 
    fragrant cloud the mercy-seat, thus touched with blood, was the figure of 
    the ceaseless intercession of our great High Priest in the holiest. &quot;For 
    Christ is not entered into the holy places made with hands, which are the 
    figures of the true: but into heaven itself now to appear in the presence of 
    God for us.&quot; <br>
    <br>
    It is an individual, an anticipative, and a present intercession. It 
    embraces all the personal needs of each believer, it precedes each 
    temptation and each trial, and at the moment that the sympathy and the 
    prayers of the Savior are the most called for, and are felt to be the most 
    soothing, it bears the saint and his sorrow on its bosom before the throne. 
    Just at a crisis of his history, at a juncture, perhaps, the most critical 
    in his history, the heart, oppressed with its emotions, cannot breathe a 
    prayer--Jesus is remembering him, sympathizing with him, and interceding for 
    him. Oh, who can fully describe the blessings that flow through the 
    intercession of the Son of God? The love, the sympathy, the forethought, the 
    carefulness, the minute interest in all our concerns, are blessings beyond 
    description. <br>
    <br>
    Tried, tempted believer! Jesus makes intercession for you, Your case is not 
    unknown to Him. Your sorrow is not hidden from Him. Your name is on His 
    heart; your burden is on His shoulder; and because He not only has prayed 
    for you, but prays for you now, your faith shall not fail. Your great 
    accuser may stand at your right hand to condemn you, but your great Advocate 
    is at the right hand of God to plead for you. And greater is He that is for 
    you, than all that are against you. The mediatorial work of Christ shuts 
    every mouth, meets every accusation, and ignores every indictment that can 
    be brought against those for whom He died, rose again, ascended up on high, 
    and makes intercession. <br>
    <br>
    <br>
