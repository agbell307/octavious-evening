    <b>MAY 1. </b><br>
    <br>
    &quot;For he is our peace, who has made both one, and has broken down the middle 
    wall of partition between us.&quot; Ephes. 2:14<br>
    <br>
    BEHIND this wall Jesus did once stand, and although thus partially obscured, 
    yet to those who had faith to see Him, dwelling though they were in the 
    twilight of the Gospel, He manifested Himself as the true Messiah, the Son 
    of God, the Savior of His people. &quot;Abraham rejoiced to see my day,&quot; says 
    Jesus, &quot;and he saw it, and was glad.&quot; But this wall no longer stands. The 
    shadows are fled, the darkness is dispersed, and the true light now shines. 
    Beware of those teachers who would rebuild this wall; and who by their 
    superstitious practices, and legal representations of the Gospel, do in 
    effect rebuild it. Remember that &quot;Christ is the end of the law for 
    righteousness to every one that believes.&quot; <br>
    <br>
    It is behind &quot;our wall&quot; that Jesus stands--the wall which we, the new 
    covenant saints, erect. Many are the separating influences between Christ 
    and His people; many are the walls which we, alas! allow to intervene, 
    behind which we cause Him to stand. What are the infidelity, I had almost 
    said atheism, the carnality, the coldness, the many sins of our hearts, but 
    so many obstructions to Christ's full and frequent manifestations of Himself 
    to our souls? But were we to specify one obstruction in particular, we would 
    mention unbelief as the great separating wall between Christ and His people. 
    This was the wall which obscured from the view of Thomas his risen Lord. And 
    while the little Church was jubilant in the new life and joy with which 
    their living Savior inspired them, he alone lingered in doubt and sadness, 
    amid the shadows of the tomb. &quot;Except I thrust my hand into His side, I will 
    not believe.&quot; Nothing more effectually separates us from, or rather obscures 
    our view of, Christ than the sin of unbelief. Not fully crediting His 
    word--not simply and implicitly relying upon His work--not trusting His 
    faithfulness and love--not receiving Him wholly and following Him fully--only 
    believing and receiving half that He says and commands--not fixing the eye 
    upon Jesus as risen and alive, as ascended and enthroned, leaving all 
    fullness, all power, all love. Oh this unbelief is a dead, towering wall 
    between our Beloved and our souls! <br>
    <br>
    And yet does He stand behind it? Does it not compel Him to depart and leave 
    us forever? Ah no! He is there! Oh wondrous grace, matchless love, infinite 
    patience! Wearied with forbearing, and yet there! Doubted, distrusted, 
    grieved, and yet standing there--His locks wet with the dew of the 
    night--waiting to be gracious, longing to manifest Himself. Nothing has 
    prevailed to compel Him to withdraw. When our coldness might have prevailed, 
    when our fleshliness might have prevailed, when our neglect, ingratitude, 
    and backslidings might have prevailed, never has He entirely and forever 
    withdrawn. His post is to watch with a sleepless eye of love the purchase of 
    His dying agonies, and to guard His &quot;vineyard of red wine night and day, 
    lest any hurt it.&quot; Who can adequately picture the solicitude, the 
    tenderness, the jealousy, with which the Son of God keeps His especial 
    treasure? And whatever would force Him to retire--whether it be the coldness 
    that congeals, or the fierce flame that would consume--yet such is His 
    deathless love for His people, &quot;He withdraws not His eyes front the 
    righteous&quot; for one moment. There stands the &quot;Friend that sticks closer than 
    a brother,&quot; waiting to beam upon them a glance of His love-enkindled eye, 
    and to manifest Himself to them as He does not unto the world, even from 
    behind our wall. <br>
    <br>
    <br>
