    NOVEMBER 3.<br>
    <br>
    &quot;But the God of all grace, who has called us unto his eternal glory by 
    Christ Jesus, after that you have suffered a while, make you perfect, 
    establish, strengthen, settle you.&quot; 1 Peter 5:10<br>
    <br>
    There is a painful forgetfulness among many of the saints of God of the 
    appointed path of believers through the world. It is forgotten that this 
    path is to be one of tribulation; that so far from being a smooth, a 
    flowery, and an easy path, it is rough, thorny, and difficult. The believer 
    often expects all his heaven on earth. He forgets that whatever spiritual 
    enjoyment there may be here, kindred in its nature to the joys of the 
    glorified--and too much of this he cannot expect--yet the present is but the 
    wilderness state of the church, and the life that now is, is but that of a 
    pilgrimage and a sojourning. Kind was our Lord's admonition, &quot;in the world 
    you shall have tribulation:&quot; and equally so that of the apostle, &quot;we must 
    through much tribulation enter into the kingdom.&quot; Affliction, in some of its 
    many and varied forms, is the allotment of all the Lord's people. If we have 
    it not, we lack the evidence of our true sonship; for the Father &quot;scourges 
    every son whom he receives.&quot; But whatever the trial or affliction is, the 
    Holy Spirit is the Comforter. And how does He comfort the afflicted soul? In 
    this way.<br>
    <br>
    He unfolds the love of his God and Father in the trial. He shows the 
    believer that his sorrow, so far from being the result of anger, is the 
    fruit of love; that it comes from the heart of God, sent to draw the soul 
    nearer to Himself, and to unfold the depths of His own grace and tenderness; 
    that whom he &quot;loves He chastens.&quot; And, oh, how immense the comfort that 
    flows into a wounded spirit, when love--deep, unchangeable, covenant love--is 
    seen in the hand that has stricken; when the affliction is traced to the 
    covenant, and through the covenant, to the heart of a covenant God.<br>
    <br>
    The Spirit comforts by revealing the end why the affliction is sent. He 
    convinces the believer that the discipline, though painful, was yet needed; 
    that the world was, perhaps, making inroads upon the soul, or creature love 
    was shutting out Jesus; some indulged sin was, perhaps, crucifying Him 
    afresh, or some known spiritual duty was neglected. The Comforter opens his 
    ears to hear the voice of the rod, and Him who had appointed it. He begins 
    to see why the Lord has smitten, why He has caused His rough wind and His 
    east wind to blow; why He has blasted, why He has wounded. And now the Achan 
    is discovered, cast out, and stoned. The heart, disciplined, returns from 
    its wanderings, and, wounded, bleeding, suffering, seeks more earnestly than 
    ever a wounded, bleeding, suffering Savior. Who can fully estimate the 
    comfort which flows from the sanctified discipline of the covenant? When the 
    end for which the trial was sent is accomplished, it may be in the discovery 
    of some departure, in the removal of an obstruction to the growth of grace, 
    of some object that obscured the glory of Jesus, and that suspended His 
    visits of love to the soul, &quot;Blessed discipline,&quot; he may exclaim, &quot;that has 
    wrought so much good--gentle chastisement, that has corrected so much 
    evil--sweet medicine, that has produced so much health!&quot;<br>
    <br>
    <br>
