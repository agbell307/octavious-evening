    NOVEMBER 7.<br>
    <br>
    &quot;For consider him that endured such contradiction of sinners against 
    himself, lest you be wearied and faint in your minds.&quot; Hebrews 12:3<br>
    <br>
    The assaults of the adversary contribute not a little to the sense of 
    weariness which often prostrates a child of God. To be set up as a mark for 
    Satan; the enemy smiting where sensibility is the keenest; assailing where 
    weakness is the greatest; taking advantage of every new position and 
    circumstance, especially of a season of trial, of a weak, nervous 
    temperament, or of a time of sickness--distorting God's character, diverting 
    the eye from Christ, and turning it in upon self--are among Satan's devices 
    for casting down the soul of a dear believer. And then, there are the 
    narrowness of the narrow way, the intricacies of the intricate way, the 
    perils of the perilous way--all tending to jade and dispirit the soul. To 
    walk in a path so narrow and yet so dangerous, that the white garment must 
    needs be closely wrapped around; to occupy a post of duty so conspicuous, 
    responsible, and difficult, as to fix every eye; some gazing with undue 
    admiration, and others with keen and cold suspicion, ready to detect and to 
    censure any slight irregularity--add not a little to the to toilsomeness of 
    the way. Notice, also, the numerous and varied trials and afflictions which 
    pave his pathway to heaven--his tenderest mercies often his acutest trials, 
    his trials often weighing him to the earth--and you have the outline of a 
    melancholy picture, of which he whose eye scans this page may be the 
    original. Does it surprise, then, that from the lips of such a one the 
    exclamation often rises, &quot;Oh that I had wings like a dove! for then would I 
    fly away, and be at rest. I would hasten my escape from the windy storm and 
    tempest.&quot;<br>
    <br>
    Remember, there will be a correspondence between the life of Christ in the 
    soul, and the life which Christ lived when he tabernacled in the flesh. The 
    indwelling of Christ in the believer is a kind of second incarnation of the 
    Son of God. When Christ enters the heart of a poor sinner, He once more 
    clothes Himself with our nature. The life which Christ lived in the days of 
    His sojourn on earth was a life of sorrow, of conflict, of temptation, of 
    desertion, of want, and of suffering in every form. Does He now live a 
    different life in the believer? No; He is still tempted and deserted, in 
    sorrow and in want, in humiliation and in suffering--in His people. What! did 
    you think that these fiery darts were leveled at you? Did you suppose that 
    it was you who were deserted, that it was you who suffered, that it was you 
    who were despised, that it was you who were trodden under foot? No, my 
    brother, it was Christ dwelling in you. All the malignity of Satan, all the 
    power of sin, and all the contempt of the world, are leveled, not against 
    you, but against the Lord dwelling in you. Were it all death in your soul, 
    all darkness, sinfulness, and worldliness, you would be an entire stranger 
    to these exercises of the renewed man.<br>
    <br>
    Behold the love and condescension of Jesus! that after all He endured in His 
    own person, He should again submit Himself to the same in the persons of His 
    saints; that He should, as it were, return, and tread again the path of 
    suffering, of trial, of humiliation, in the life which each believer lives. 
    Oh, how it speaks that love which passes knowledge! How completely is Christ 
    one with His saints! and yet, how feebly and faintly do we believe this 
    truth! How little do we recognize Christ in all that relates to us! and yet 
    He is in all. He is in every providence that brightens or that darkens upon 
    our path. &quot;Christ is all, and in all.&quot;<br>
    <br>
    <br>
