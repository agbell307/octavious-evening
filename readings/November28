    NOVEMBER 28.<br>
    <br>
    &quot;Though I speak with the tongues of men and of angels, and have not love, I 
    am become as sounding brass, or a tinkling cymbal. And though I have the 
    gift of prophecy, and understand all mysteries, and all knowledge; and 
    though I have all faith, so that I could remove mountains, and have not 
    love, I am nothing. And though I bestow all my goods to feed the poor, and 
    though I give my body to be burned, and have not love, it profits me 
    nothing.&quot; 1 Corinthians 13:1-3<br>
    <br>
    There is no truth more distinctly uttered or more emphatically stated than 
    this--the infinite superiority of love to gifts. And in pondering their 
    relative position and value, let it be remembered, that the gifts which are 
    here placed in competition with grace are the highest spiritual gifts. Thus 
    does the apostle allude to them: &quot;God has set some in the church, first 
    apostles, secondarily prophets, thirdly teachers, after that miracles, then 
    gifts of healing.&quot; Then follows the expressive declaration of our motto. In 
    other words, &quot;Though I were an apostle, having apostolic gifts; though I 
    were a prophet, possessed of prophetic gifts; or though I were an angel, 
    clothed with angelic gifts; yet, destitute of the grace of love, my religion 
    were but as an empty sound, nothing worth.&quot; Is there in all this any 
    undervaluing of the spiritual gifts which the great exalted Head of the 
    church has bestowed upon His ministers? Far from it. The apostle speaks of 
    the way of spiritual gifts as excellent, but existing alone, they cannot 
    bring the soul to heaven. And love may exist apart from gifts; but where 
    love is found, even alone, there is that most excellent grace, that will 
    assuredly conduct its possessor to glory. &quot;Grace embellished with gifts is 
    the more beautiful; but gifts without grace are only a richer spoil for 
    Satan.&quot;<br>
    <br>
    And why this superiority of the grace of love? Why is it so excellent, so 
    great, so distinguished? Because God's love in the soul is a part of God 
    Himself; for &quot;God is love.&quot; It is as it were a drop of the essence of God 
    falling into the heart of man. &quot;He that dwells in love, dwells in God, and 
    God in him.&quot; This grace of love is implanted in the soul at the period of 
    its regeneration. The new creature is the restoration of the soul to God, 
    the expulsion from the heart of the principle of enmity, and the flowing 
    back of its affections to their original center. &quot;Every one that loves is 
    born of God.&quot; Is it again asked, why the love of His saints is so costly in 
    God's eye? Because it is a small fraction of the infinite love which He 
    bears towards them. Does God delight Himself in His love to His church? Has 
    He set so high a value upon it, as to give His own Son to die for it? Then, 
    wherever He meets with the smallest degree of that love, He must esteem it 
    more lovely, more costly, and more rare, than all the most splendid gifts 
    that ever adorned the soul. &quot;We love Him because He first loved us.&quot;<br>
    <br>
    Here, then, is that grace in the soul of man which more than all others 
    assimilates him to God. It comes from God, it raises the soul to God, and it 
    makes the soul like God. How encouraging, then, to know the value which the 
    Lord puts upon our poor returns of love to Him! Of gifts we may have none, 
    and even of love but little; yet of that little, who can unfold God's 
    estimate of its preciousness! He looks upon it as a little picture of 
    Himself. He sees in it a reflection--dim and imperfect indeed--of His own 
    image. As He gazes upon it, He seems to say--&quot;Your parts, my child, are 
    humble, and your gifts are few; your knowledge is scanty, and your tongue is 
    stammering; you can not speak for me, nor pray to me in public, by reason of 
    the littleness of your attainments, and the greatness of your infirmity; but 
    you do love me, my child, and in that love, which I behold, I see my nature, 
    I see my heart, I see my image, I see myself; and that is more precious to 
    me than all besides.&quot; Most costly to Him also are all your labors of love, 
    your obedience of love, your sacrifices of love, your offerings of love, and 
    your sufferings of love. Yes, whatever blade or bud, flower or fruit, grows 
    upon the stem of love, it is most lovely, and precious, and fragrant to God.<br>
    <br>
    <br>
