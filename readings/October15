    OCTOBER 15.<br>
    <br>
    &quot;He looks upon men, and if any say, I have sinned, and perverted that which 
    was right, and it profited me not; he will deliver his soul from going into 
    the pit, and his life shall see the light.&quot; Job 33:27, 28<br>
    <br>
    Let the child of God be encouraged to take all his sins to his heavenly 
    Father. Have you sinned? Have you taken a single step in departure from God? 
    Is there the slightest consciousness of guilt? Go at once to the throne of 
    grace; stay not until you find some secret place for confession--stay not 
    until you are alone; lift up your heart at once to God, and confess your sin 
    with the hand of faith upon the great, atoning Sacrifice. Open all your 
    heart to Him. Do not be afraid of a full and honest confession. Shrink not 
    from unfolding its most secret recesses--lay all bare before His eyes. Do you 
    think He will turn from the exposure? Do you think He will close His ear 
    against your breathings? Oh no! Listen to His own encouraging, persuasive 
    declarations--&quot;Go and proclaim these words towards the north, and say, 
    Return, you backsliding Israel, says the Lord; and I will not cause mine 
    anger to fall upon you: for I am merciful, says the Lord; and I will not 
    keep anger forever. Only acknowledge your iniquity that you have 
    transgressed against the Lord your God.&quot; &quot;I will heal their backsliding; I 
    will love them freely; for mine anger is turned away from him.&quot; Oh, what 
    words are these! Does the eye of the poor backslider fall on this page? And 
    as he now reads of God's readiness to pardon--of God's willingness to receive 
    back the repenting prodigal--of His yearning after His wandering child--feels 
    his heart melted, his soul subdued, and, struck with that amazing 
    declaration, &quot;Only acknowledge your iniquity,&quot; would dare creep down at His 
    feet, and weep, and mourn, and confess. Oh! is there one such now reading 
    this page? then return, my brother, return! God--the God against whom you 
    have sinned--says, &quot;Return.&quot; Your Father--the Father from whom you have 
    wandered--is looking out for the first return of your soul, for the first 
    kindlings of godly sorrow, for the first confession of sin. God has not 
    turned His back upon you, though you have turned your back upon Him. God has 
    not forgotten to be gracious, though you have forgotten to be faithful. &quot;I 
    remember you&quot;--is His own touching language--&quot;the kindness of your youth, the 
    love of your espousals.&quot; Oh! then, come back; this moment, come back; the 
    fountain is still open--Jesus is still the same--the blessed and eternal 
    Spirit, loving and faithful as ever--God ready for pardon: take up, then, the 
    language of the prodigal and say, &quot;I will arise and go to my Father, and 
    will say unto him, Father, I have sinned against heaven and in Your sight, 
    and am no more worthy to be called Your son.&quot; &quot;If we confess our sins, He is 
    faithful and just to forgive us our sins, and to cleanse us from all 
    unrighteousness.&quot; <br>
    <br>
    The blessings that result from a strict observance of daily confession of 
    sin are rich and varied. We would from the many specify two. The conscience 
    retains its tender susceptibility of guilt. Just as a breath will tarnish a 
    mirror highly polished, so will the slightest aberration of the heart from 
    God--the smallest sin--leave its impression upon a conscience in the habit of 
    a daily unburdening itself in confession, and of a daily washing in the 
    fountain. Going thus to God, and acknowledging iniquity over the head of 
    Immanuel--pleading the atoning blood--the conscience retains its tenderness, 
    and sin, all sin, is viewed as that which God hates, and the soul abhors.
    <br>
    <br>
    This habit, too, keeps, so to speak, a clear account between God and the 
    believer. Sins daily and hourly committed are not forgotten; they fade not 
    from the mind, and therefore they need not the correcting rod to recall them 
    to remembrance. For let us not forget, God will eventually bring our sins to 
    remembrance; &quot;He will call to remembrance the iniquity.&quot; David had forgotten 
    his sin against God, and his treacherous conduct to Uriah, until God sent 
    the prophet Nathan to bring his iniquity to remembrance. A daily confession, 
    then, of sin, a daily washing in the fountain, will preserve the believer 
    from many and, perhaps, deep afflictions. This was David's testimony--&quot;I 
    acknowledged my sin unto You, and mine iniquity have I not hid. I said, I 
    will confess my transgression unto the Lord, and You forgave the iniquity of 
    my sin.&quot;<br>
    <br>
    <br>
