    OCTOBER 22.<br>
    <br>
    &quot;Sanctified by the word of God.&quot; 1 Timothy 4:5<br>
    <br>
    It is the natural tendency of Divine truth, when received into the heart, to 
    produce holiness. The design of the whole plan of redemption was to secure 
    the highest holiness and happiness of the creature; and when the gospel 
    comes with the power of God unto the salvation of the soul, this end is 
    preeminently secured. The renewed man is a pardoned man; the pardoned man 
    becomes a holy man; and the holy man is a happy man. Look, then, at God's 
    word, and trace the tendency of every doctrine, precept, promise, and 
    threatening, and mark the holy influence of each. Take the doctrine of God's 
    everlasting love to His people, as seen in their election to eternal life. 
    How holy is the tendency of this truth! &quot;Blessed be the God and Father of 
    our Lord Jesus Christ, who has blessed us with all spiritual blessings in 
    heavenly places in Christ; according as He has chosen us in Him before the 
    foundation of the world, that we should be holy and without blame before Him 
    in love.&quot; Let not my reader turn from this glorious doctrine, because he may 
    find it irreconcilable with others that he may hold, or because the mists of 
    prejudice may long have veiled it from his mind; it is a revealed doctrine, 
    and therefore to be fully received; it is a holy doctrine, and therefore to 
    be ardently loved. Received in the heart by the teaching of the Holy Spirit, 
    it lays the pride of man in the dust, knocking from beneath the soul all 
    ground for self-glorying, and expands the mind with the most exalted views 
    of the glory, grace, and love of Jehovah. He who receives the doctrine of 
    electing love in his heart by the power of the Spirit, bears about with him 
    the material of a holy walk; its tendency is to humble, abase, and sanctify 
    the man. <br>
    <br>
    Thus holy, too, is the revealed doctrine of God's free, sovereign, and 
    distinguishing grace. The tendency of this truth is most sanctifying: for a 
    man to feel that God alone has made him to differ from another--that what he 
    has, he has received--that by the free, distinguishing grace of God he is 
    what he is--is a truth, when experienced in the heart, surely of the most 
    holy influence. How it lays the axe at the root of self! how it stains the 
    pride of human glory, and hushes the whispers of vain boasting! It lays the 
    renewed sinner where he ought ever to lie, in the dust; and places the 
    crown, where it alone ought to shine, bright and glorious, upon the head of 
    sovereign mercy. &quot;Lord, why me? I was far from You by wicked works; I was 
    the least of my Father's house, and, of all, the most unworthy and unlikely 
    object of Your love and yet Your mercy sought me--Your grace selected me out 
    of all the rest, and made me a miracle of its omnipotent power. Lord, to 
    what can I refer this, but to Your mere mercy, Your sovereign and free 
    grace, entirely apart from all worth or worthiness that You did see in me? 
    Take, therefore, my body, soul, and spirit, and let them be, in time and 
    through eternity, a holy temple to Your glory.&quot; <br>
    <br>
    All the precepts, too, are on the side of holiness. &quot;If you love me, keep my 
    commandments;&quot; &quot;Be you holy, for I am holy;&quot; &quot;Come out of the world and be 
    you separate, and touch not the unclean thing.&quot;' &quot;God has not called us unto 
    uncleanness, but unto holiness;&quot; &quot;That you might walk worthy of the Lord 
    unto all pleasing, being fruitful in every good work, and increasing in the 
    knowledge of God.&quot; Holy precepts! May the eternal Spirit engrave them deep 
    upon our hearts. <br>
    <br>
    Not less sanctifying in their tendency are the &quot;exceeding great and precious 
    promises&quot; which the word of truth contains. &quot;Having, therefore these 
    promises, dearly beloved, let us cleanse ourselves from all filthiness of 
    the flesh and spirit, perfecting holiness in the fear of God.&quot; <br>
    <br>
    Thus holy and sanctifying are the nature and the effect of Divine truth. It 
    is in its nature and properties most holy; it comes from a holy God and 
    whenever and wherever it is received in the heart, as the good and 
    incorruptible seed of the kingdom, it produces that which is in accordance 
    with its own nature--HOLINESS. As is the tree, so are the fruits; as is the 
    cause, so are the effects. It brings down and lays low the high thoughts of 
    man, by revealing to him the character of God; it convinces him of his deep 
    guilt and awful condemnation, by exhibiting the Divine law; it unfolds to 
    him God's hatred of sin, His justice in punishing and His mercy in pardoning 
    it, by unfolding to his view the cross of Christ; and taking entire 
    possession of the soul, it implants new principles, supplies new motives, 
    gives a new end, begets new joys, and inspires new hopes--in a word, diffuses 
    itself through the whole moral man, changes it into the same image, and 
    transforms it into &quot;an habitation of God through the Spirit.&quot;<br>
    <br>
    <br>
