    SEPTEMBER 7.<br>
    <br>
    &quot;Holding faith, and a good conscience, which some having put away, 
    concerning faith have made shipwreck.&quot; 1 Timothy 1:19<br>
    <br>
    Faith is an essential part of the spiritual armor: &quot;Above all, taking the 
    shield of faith, with which you shall be able to quench the fiery darts of 
    the wicked.&quot; Faith is also spoken of as the believer's breastplate: &quot;But let 
    us, who are of the day, be sober, putting on the breastplate of faith.&quot; 
    There is not a moment, even the holiest, but we are exposed to the &quot;fiery 
    darts&quot; of the adversary. The onset, too, is often at a moment when we least 
    suspect its approach; seasons of peculiar nearness to God, of hallowed 
    enjoyment--&quot;for we wrestle not against flesh and blood, but against 
    principalities, against powers, against the rulers of the darkness of this 
    world, against spiritual wickedness in high places&quot;--are frequently selected 
    as the occasion of attack. But, clad in this armor--the shield and the 
    breastplate of faith--no weapon formed against us shall prosper; no &quot;fiery 
    dart&quot; shall be quenched, and the enemy shall be put to flight. Faith in a 
    crucified, risen, conquering, exalted Savior--faith in a present and 
    ever-living Head--faith eyeing the crown glittering, and the palm waving in 
    its view, is the faith that overcomes and triumphs. Faith, dealing 
    constantly and simply with Jesus, flying to His atoning blood, drawing from 
    His fullness, and at all times and under all circumstances looking unto Him, 
    will ever bring a conflicting soul off more than conqueror. &quot;This is the 
    victory that overcomes the world, even our faith. Who is He that overcomes 
    the world, but he that believes that Jesus is the Son of God?&quot; <br>
    <br>
    Faith is a purifying grace: &quot;Purifying their hearts by faith.&quot; It is a 
    principle holy in its nature and tendency: he is most holy who has most 
    faith; he who has least faith is most exposed to the assaults of his inbred 
    corruptions. If there is in any child of God a desire for Divine conformity, 
    for more of the Spirit of Christ, more weanedness, and crucifixion, and 
    daily dying, this should be his ceaseless prayer--&quot;Lord, increase my faith.&quot; 
    Faith in Jesus checks the power of sin, slays the hidden corruption, and 
    enables the believer to &quot;endure as seeing Him who is invisible.&quot; <br>
    <br>
    Nothing, perhaps, more secretly and effectually militates against the vigor 
    of a life of faith, than the power of unsubdued sin in the heart. Faith, as 
    we have just seen, is a holy indwelling principle; it has its root in the 
    renewed, sanctified heart; and its growth and fruitfulness depend much upon 
    the progressive richness of the soil in which it is embedded: if the noxious 
    weeds of the natural soil are allowed to grow and occupy the heart, and gain 
    the ascendancy, this celestial plant will necessarily droop and decay. In 
    order to form some conception of the utter incongruity of a life of faith 
    with the existence and power of unmortified sin in the heart, we have but to 
    imagine the case of a believer living in the practice of unsubdued sin. What 
    is the real power of faith in him? where is its strength? where are its 
    glorious achievements? We look for the fruit of faith--the lowly, humble, 
    contrite spirit--the tender conscience--the traveling daily to the atoning 
    blood--the living upon the grace that is in Christ Jesus--the carrying out of 
    Christian principle--crucifixion to the world--patient submission to a life of 
    suffering--meek resignation to a Father's discipline--a constant and vivid 
    realization of eternal realities--we look for these fruits of faith, but we 
    find them not. And why? Because there is the worm of unmortified sin feeding 
    at the root; and, until that is slain, faith will always be sickly, 
    unfruitful, and &quot;ready to die.&quot;<br>
    <br>
    A looking off of Christ will tend greatly to the weakening and 
    unfruitfulness of faith. It is said, that the eaglet's eye becomes strong 
    through the early discipline of the parent; placed in such a position when 
    young, as to fix the gaze intently upon the sun, the power of vision 
    gradually becomes so great, as to enable it in time to look at its meridian 
    splendor without uneasiness, and to observe the remotest object without 
    difficulty. The same spiritual discipline strengthens the eye of faith; the 
    eye grows vigorous by looking much at the Sun of Righteousness. The more 
    constantly it gazes upon Jesus, the stronger it grows; and the stronger it 
    grows, the more glory it discovers in Him, the more beauty in His person, 
    and perfection in His work. Thus strengthened, it can see things that are 
    afar off--the promises of a covenant-keeping God, the hope of eternal life, 
    the crown of glory; these it can look upon and almost touch. &quot;Faith is the 
    substance of things hoped for, the evidence of things not seen.&quot; O precious, 
    costly grace of the Eternal Spirit! who would not possess you? who would not 
    mortify everything that would wound, enfeeble, and cause you to decay in the 
    soul?<br>
    <br>
    <br>
