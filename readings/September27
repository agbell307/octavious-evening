    SEPTEMBER 27.<br>
    <br>
    &quot;Be merciful unto me, O God, be merciful unto me: for my soul trusts in you: 
    yes, in the shadow of your wings will I make my refuge, until these 
    calamities be overpast. I will cry unto God most high; unto God that 
    performs all things for me.&quot; Psalm 57:1, 2<br>
    <br>
    The exercise of faith strengthens, as the neglect to exercise, weakens it. 
    It is the constant play of the arm that brings out its muscular power in all 
    its fullness; were that arm allowed to hang by its own side, still and 
    motionless, how soon would its sinews contract, and its energy waste away! 
    So it is with faith, the right arm of a believer's strength; the more it is 
    exercised, the mightier it becomes; neglect to use it, allow it to remain 
    inert and inoperative, and the effect will be a withering up of its power. 
    Now when gloomy providences, and sharp trials and temptations, thicken 
    around a poor believing soul, then is it the time for faith to put on its 
    strength, and come forth to the battle. God never places His child in any 
    difficulties, or throws upon him any cross, but it is a call to exercise 
    faith; and if the opportunity of its exercise passes away without 
    improvement, the effect will be a weakening of the principle, and a feeble 
    putting forth of its power in the succeeding trial. Do not forget, that the 
    more faith is brought into play, the more it increases; the more it is 
    exercised, the stronger it becomes. <br>
    <br>
    Some of the choicest mercies of the covenant brought into the experience of 
    the believer, come by a travail of faith: it maybe a tedious and a painful 
    process; faith may be long and sharply tried, yet the blessings it will 
    bring forth will more than repay for all the weeping, and suffering, and 
    crying, it has occasioned. Do not be surprised, then, at any severe trial of 
    faith; be sure that when it is thus tried, God is about to bring your soul 
    into the possession of some great and perhaps hitherto unexperienced mercy. 
    It may be a travail of faith for spiritual blessing; and the result may be a 
    deepening of the work in your heart, increase of spirituality, more 
    weanedness from creature-trust, and more child-like leaning upon the Lord; 
    more simple, close, and sanctifying knowledge of the Lord Jesus. Or, it may 
    be a travail of faith for temporal mercy, for the supply of some need, the 
    rescue from some embarrassment, the deliverance out of some peculiar and 
    trying difficulty; but whatever the character of the trial of faith be, the 
    issue is always certain and glorious. The Lord may bring His child into 
    difficult and strait paths, He may hedge him about with thorns so that he 
    cannot get out, but it is only to draw the soul more simply to repose in 
    Himself; that, in the extremity, when no creature would or could help, when 
    refuge failed, and no man cared for his soul, that then faith should go out 
    and rest itself in Him who never disowns His own work, but always honors the 
    feeblest exhibition, and turns His ear to the faintest cry. &quot;Out of the 
    depths have I cried unto You, O Lord. Lord, hear my voice; let Your ears be 
    attentive to the voice of my supplication.&quot; &quot;In my distress I called upon 
    the Lord, and cried unto my God: He heard my voice out of His temple, and my 
    cry came before Him, even into His ears.&quot; &quot;O magnify the Lord with me, and 
    let us exalt His name together. I sought the Lord, and He heard me, and 
    delivered me from all my fears.&quot; &quot;This poor man cried, and the Lord heard 
    him; and saved him out of all his troubles.&quot; Here was the severe travail of 
    faith, and here we see the blessed result. Thus true is God's word, which 
    declares that &quot;weeping may endure for a night, but joy comes in the 
    morning.&quot; <br>
    <br>
    The trial of faith is a test of its degree. We know not what faith we 
    possess, until the Lord calls it into exercise; we may be greatly deceived 
    as to its nature and degree; to walk upon the stormy water may be thought by 
    us an easy thing; to witness for Christ, no hard matter: but the Lord brings 
    our faith to the test. He bids us come to Him upon the water, and then we 
    begin to sink; He suffers us to be assailed by our enemies, and we shrink 
    from the cross; He puts our faith to the trial, and then we learn how little 
    we possess.<br>
    <br>
    <br>
